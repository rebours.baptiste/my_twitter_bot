### Imports
import tweepy
import time
import re



### Constants to manage tweets
WORDS_REPLY = ["rt avec", "tweet avec", "commente", "commentaire"] # to identify which tweets to reply to
DONT_PARTICIPATE = ["-tag", "-contains:mention", "-contains:identifie", "-contains:invite", "-is:retweet", "-is:quote", "-is:reply"] # to identify which tweets to not participate to
HASHTAGS_TO_AVOID = ["concours", "rt", "retweet", "fav", "aime", "like", "jeu", "jeuconcours", "giveaway"] # hashtags to avoid in replies



### Participate to concours
def process_tweets(api, ids_processed, friends, nb_tweets=1000):
    for search_tweet in tweepy.Cursor(api.search_tweets, q='(concours OR follow) rt'+' '+' '.join(DONT_PARTICIPATE), tweet_mode='extended', lang='fr').items(nb_tweets):
        # Get tweet info - either the initial tweet if search_tweet is a RT, either search_tweet
        try:
            tweet_id = search_tweet.retweeted_status.id_str
            tweet_text = search_tweet.retweeted_status.full_text
            tweet_username = search_tweet.retweeted_status.user.screen_name
            already_rt = search_tweet.retweeted_status.retweeted
        except:
            tweet_id = search_tweet.id_str
            tweet_text = search_tweet.full_text
            tweet_username = search_tweet.user.screen_name
            already_rt = search_tweet.retweeted
        # Participate : RT + Fav + Follow (+ reply if necessary)
        if tweet_id not in ids_processed and not already_rt : 
            # Actions
            # - RT
            try:
                api.retweet(tweet_id) 
            except tweepy.errors.Forbidden :
                print('Already RTed.\n')
            # - Like
            try:
                api.create_favorite(tweet_id) 
            except tweepy.errors.Forbidden :
                print('Already liked.\n')
            # - Follow
            users_to_follow = [user for user in list(set([username for username in re.findall(r"@(\w+)", tweet_text)] + [tweet_username])) if user not in friends]
            for user in users_to_follow: 
                try:
                    api.create_friendship(screen_name=user)
                    friends.add(user)
                except:
                    print('User not found.\n')
            # - Reply
            if any(token_reply in tweet_text.lower() for token_reply in WORDS_REPLY):
                hashtags_to_mention = [hashtag for hashtag in re.findall(r"#(\w+)", tweet_text) if hashtag.lower() not in HASHTAGS_TO_AVOID]
                try:
                    status_reply = '@' + tweet_username + ' ' + ' '.join(['#'+hashtag for hashtag in hashtags_to_mention])
                    api.update_status(status = status_reply, in_reply_to_status_id = tweet_id)
                    replied = f"Replied with {' '.join(['#'+hashtag for hashtag in hashtags_to_mention])}\n------------------"
                except:
                    replied = f'Did not reply because : ID {tweet_id} tweet or @{tweet_username} not existing.\n'
            else:
                replied = f'Did not reply because : no word of the tweet is in WORDS_REPLY.\n'
            # Result
            print(f"------------------\nRT and liked\n---\n{tweet_text}\n---\n")
            if users_to_follow != []:
                print(f"Followed {' '.join(['@'+user for user in users_to_follow])}\n")
            print(replied)
            ids_processed.add(tweet_id)
        else:
            print(f"------------------\nID {tweet_id} tweet already processed.\n------------------")
        time.sleep(5)


