# my_twitter_bot

Bot to participate to RT+follow concours on Twitter. 

Used with @Henchman_800 account (https://twitter.com/henchman_800).


## Requirements

- A Twitter developer account 
- A Twitter project (and associate keys)
- Python 3.9
- Tweepy
- Optionnaly : an AWS account (to deploy it)


## Files

### init_bot.py
Connecting to Twitter API with the Twitter project and getting list of already processed tweets' IDs and friends, in order to avoid useless processing. 

Set your environment variables with the corresponding keys and values before using create_api() function.

### participate_concours.py
Get a list of tweets corresponding to concours ("rt follow" or "concours rt") with search_tweets() API function.

RT and like all of these tweets if not already done, and follow each user associated to these tweets if not already done.

Optionnaly replies to the tweet if it specifies it (with a # for example), except if it specifies to tag friends (to avoid account suspension).

### main.py
Connecting to API and participating to concours.



## Deploying
Daily run thanks to AWS Lambda and AWS Function.
For more information, look at https://www.commentcoder.com/bot-twitter/.
