### Imports
from init_bot import create_api, load_idsprocessed_friends
from participate_concours import process_tweets


### Process
api = create_api()
ids_processed, friends = load_idsprocessed_friends(api)
process_tweets(api, ids_processed, friends, nb_tweets=25)
