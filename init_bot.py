### Imports
import os
import tweepy



### Connection to API
def create_api():
    # Getting API keys
    API_KEY = os.getenv('API_KEY')
    API_SECRET_KEY = os.getenv('API_SECRET_KEY')
    ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
    ACCESS_TOKEN_SECRET = os.getenv('ACCESS_TOKEN_SECRET')
    # Connection
    auth = tweepy.OAuthHandler(API_KEY, API_SECRET_KEY)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth, wait_on_rate_limit=True)
    try:
        api.verify_credentials()
        print("Authentication OK")
    except:
        print("Error during authentication")

    return api



### Getting processed tweets id and friends
def load_idsprocessed_friends(api):
    # processed tweets id
    ids_processed = []
    for tweet in tweepy.Cursor(api.user_timeline).items():
        try :
            ids_processed.append(tweet.retweeted_status.id_str)
        except :
            continue
    # friends
    friends = api.get_friend_ids(screen_name=api.verify_credentials().screen_name)
    
    return set(ids_processed), set(friends)

